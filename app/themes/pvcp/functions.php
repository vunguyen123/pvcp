<?php
/**
 * Function
 *
 * @package Function
 */

define('APP_PATH',dirname(__FILE__));
/**
 * CORE Includes
 **/
include APP_PATH.'/inc/Define.php';
include APP_PATH.'/inc/Setup.php';
//include APP_PATH.'/inc/plugins/acf.php';

// add file admin style css
//function my_admin_style() {
//    wp_enqueue_style( 'admin-style', get_template_directory_uri() . '/assets/css/admin-style.css' );
//}
//add_action( 'admin_enqueue_scripts', 'my_admin_style');