<?php get_header();
$id_bloc = $_GET['id'];
$id_taxonomy = get_queried_object()->term_id;

?>
	<main id="content" role="main">

	<?php echo do_shortcode('[bloc_listing act=abcl id='.$id_bloc.']'); ?>
	</main>

<script>
     jQuery ( document ).ready(function() {
        var id_taxonomy = '<?php echo $id_taxonomy; ?>';
         jQuery('.dropdown-radio .list-radio li input').each(function () {
             var check = jQuery(this).val();

             if(check == id_taxonomy){
                 jQuery(this).parent().find('label').trigger( "click" );
             }
         });

     });
</script>

<?php get_footer(); ?>