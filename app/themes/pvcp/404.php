<?php get_header();
$title     = get_field( 'title_404', 'option' );
$sub_title = get_field( 'sub_title_404', 'option' );
?>

    <div class="wrapper">
        <div class="breadcrumb">
            <div class="wrapper">
                <ul class="breadcrumbs">
                    <li><a href="<?= get_home_url(); ?>"> <?php _e('Home', 'Sciforma'); ?> </a></li>
                    <li><?php echo '404'; ?></li>
                </ul>

            </div>
        </div>
        <article class="not-found post" id="post-0">
			<?php if ( $title ): ?>
                <div class="header">
                    <h1 class="entry-title" itemprop="name"><?= $title ?></h1>
                </div>
			<?php endif; ?>
			<?php if ( $sub_title ): ?>
                <div class="entry-content" itemprop="mainContentOfPage">
                    <p> <?php _e( 'Nothing found for the requested page. Try a search instead?', 'Sciforma' ) ?></p>
                </div>
			<?php endif; ?>
        </article>
    </div>
<?php get_footer(); ?>