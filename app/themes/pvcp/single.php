<?php
/**
 * Single
 *
 * @package Single
 */
get_header();
$id_post = get_the_ID();

$bnt_text   = get_field( 'button_text_postulera_loffre' );
$btn_link   = get_field( 'button_link_postuler_a_loffre' );
$email_to   = get_field( 'email' );
$thunb_post = get_the_post_thumbnail_url( get_the_ID(), 'image-post-detail' );

$group_download = get_field( 'button_right_column' );
$title_download = $group_download['title_telecharger_la_brochure'];
$text_download  = $group_download['text_telecharger_la_brochure'];
$link_download  = $group_download['link_telecharger_la_brochure'];
$list_post      = get_field( 'list_post_type' );

$bloc_location = get_field( 'bloc_custom_location' );

$country = get_field( 'country' );

$posts_group     = $list_post['choose_post'];
$count_post_list = count( $posts_group );

$bloc_custom_location = get_field( 'bloc_custom_location' );
$url_image_location   = $bloc_custom_location['image'];

$list_title_and_icon_location = $bloc_custom_location['list_title_and_icon'];

$url_image_top = get_field( 'image_on_top_content' );

$post_type = get_post_type( $post->ID );
$taxonmy   = 'category_' . $post_type;
$terms     = get_the_terms( get_the_ID(), $taxonmy );

?>


    <div class="breadcrumb">
        <div class="wrapper">
            <ul class="breadcrumbs">
                <li><a href="<?= get_home_url(); ?>"> <?php _e('Home', 'Sciforma'); ?> </a></li>
                <li><?php the_title() ?></li>
            </ul>

        </div>
    </div>

<?php if ( sizeof( $terms ) > 0 ): ?>
    <div class="block-tags mt-tag">
        <div class="wrapper">
            <ul class="tags">
				<?php foreach ( $terms as $term ): ?>
                    <li>
                        <a href="<?= get_term_link( $term ) ?>?id=<?= get_field( 'id_bloc_listing' ) ?>"><?= $term->name ?></a>
                    </li>
				<?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>

    <div class="banner banner-cms banner__full">
        <div class="wrapper">
            <div class="banner__text">
                <h2 class="title"><?php the_title() ?></h2>
				<?php

				$time = get_the_date( 'j F, Y - H:i' );
				if($post_type == 'events') {
					$time = get_field('time_start');
				}
				$time          = str_replace( ":", "h", $time );
				$tim_bloc_post = get_the_date( 'j F, Y' );
				$location      = get_field( 'location' );
				$country       = get_field( 'country' );

				if ( $post_type == 'press_articles' ) { ?>
                    <span class="date"> <?= $time ?> </span>
					<?php
				}

				if ( $post_type == 'blog_post' || $post_type == 'press_releases' ) { ?>
                    <span class="date"> <?= $tim_bloc_post ?> </span>
					<?php
				}

				if ( $post_type == 'events' && $country ) { ?>
                    <span class="date"> <?= $time . " - " . $country ?> </span>
					<?php
				}
				if ( $post_type == 'events' && ! $country ) { ?>
                    <span class="date"> <?= $time ?> </span>
					<?php
				}
				if ( $post_type == 'job_offers' ) {

					$time = $country . " - " . get_field( 'localization_2' );

					?>
                    <span class="date"> <?= $time ?> </span>
					<?php
				}
				?>

				<?php $ex = $post->post_excerpt; ?>
				<?php if ( $ex ): ?>
                    <p><?= $ex ?></p>
				<?php endif; ?>
				<?php if ( $post_type != 'job_offers' && $bnt_text && $btn_link ): ?>
                    <p>
                        <a href="<?= $btn_link ?>"
                           class="btn btn-large btn-icon btn-icon-arrow btn-blue"><?= $bnt_text ?></a>
                    </p>
				<?php endif; ?>

				<?php if ( $post_type == 'job_offers' ): ?>
                    <p>
                        <a href="mailto:<?= $email_to ?>"
                           class="btn btn-large btn-icon btn-icon-arrow btn-blue"><?= $bnt_text ?></a>
                    </p>
				<?php endif; ?>
            </div>
        </div>
    </div>

    <div class="wrapper wysiwyg-cms">
        <div class="row-col">
            <div class="row-col__left">

                <div class="wysiwyg-info-images">
					<?php if ( $url_image_top ): ?>
                        <div class="image">
                            <img src="<?= $url_image_top ?>" alt="<?= get_the_title() ?>">
                        </div>
					<?php endif; ?>

					<?php

					if ( $post_type == 'case_studies' ) {

						get_template_part( 'template-parts/single/list-logo' );
					}
					?>

                </div>

                <div class="wysiwyg-content">
					<?php the_content(); ?>

                    <div class="wrap-social">
                        <h3><?php _e( 'Share the article ', 'Sciforma' ) ?></h3>
						<?php echo sharethis_inline_buttons(); ?>
                    </div>
					<?php

					if ( $post_type == 'blog_post' ) {
						get_template_part( 'template-parts/single/author' );
					}
					?>
                </div>
            </div>
            <div class="row-col__right sidebar" id="sidebar">
                <div class="sidebar__inner">

					<?php get_template_part( 'template-parts/single/sidebar/relationship' ); ?>

                    <div class="form-email-search">
						<?php
						$bloc_newsletter      = get_field( 'bloc_newsletter', 'option' );
						$title_newsletter     = $bloc_newsletter['title_newsletter'];
						$sub_title_newsletter = $bloc_newsletter['sub_title_newsletter'];
						$short_code           = $bloc_newsletter['contact_form'];
						?>
						<?php if ( $title_newsletter ): ?>
                            <h3><?= $title_newsletter ?></h3>
						<?php endif; ?>
						<?php if ( $sub_title_newsletter ): ?>
                            <p><?= $sub_title_newsletter ?></p>
						<?php endif; ?>
                        <div class="form-group">
							<?php echo do_shortcode( $short_code ); ?>
                            <button class="btn-submit"></button>
                        </div>
                    </div>

                    <h3 class="title"><?= $title_download ?></h3>
					<?php if ( $text_download && $link_download ): ?>
                    <p>
                        <a href="<?= $link_download ?>" class="btn btn-outline-orange"><?= $text_download ?></a>
                    <p>
						<?php endif; ?>


                    <div class="slider-cms-sidebar">
                        <div class="wrapper">
                            <div class="slider-items">

								<?php
								if ( $posts_group ):

									$i = 0;
									$j = 0;

									foreach ( $posts_group as $post_choose ) {
										$i ++;
										if ( $i == 1 || $i == $j ) {
											$j = $i + 4;
											?>


                                            <div class="item">
                                            <div class="title">
                                                <h3><?php _e( 'Useful content', 'Sciforma' ) ?></h3>
                                            </div>
                                            <div class="content">
                                            <ul>
										<?php } ?>


                                        <li>
                                            <a href="#" class="link-more"><?= $post_choose->post_title; ?></a>
                                        </li>

										<?php if ( $i % 4 == 0 || $i == $count_post_list ) {
											?>
                                            </ul>
                                            </div>
                                            </div>
											<?php

										} ?>


									<?php } endif; ?>


                                <div class="slider-controls">
                                    <button class="slick-prev slick-arrow"></button>
                                    <div class="dot"></div>
                                    <button class="slick-next slick-arrow"></button>
                                </div>
                            </div>
                        </div>
                    </div>


					<?php get_template_part( 'template-parts/single/sidebar/search' ); ?>

                    <div class="wrap-social visible-lg">
						<?php
						$title_socials = get_field( 'title_socials', 'option' );
						?>
						<?php if ( $title_socials ) {
							?>
                            <h4><?= $title_socials ?></h4>
							<?php
						} ?>

                        <ul class="social ">
							<?php

							if ( have_rows( 'socials', 'option' ) ):

								while ( have_rows( 'socials', 'option' ) ) : the_row();
									$class = get_sub_field( 'name_icon_social' );
									$link  = get_sub_field( 'link_social' );
									?>
                                    <li><a href="<?= $link ?>" target="_blank"><i class="<?= $class ?>"></i></a>
                                    </li>
								<?php
								endwhile;
							endif;

							?>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php
$shortcode = get_field( 'shortcode_publications' );
echo do_shortcode( $shortcode );
?>
<?php get_footer(); ?>