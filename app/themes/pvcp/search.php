<?php
/**
 * Search
 *
 * @package Search
 */

get_header();
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$loop  = new WP_Query( array(
	'post_type'      => array(
		'blog_post',
		'press_releases',
		'press_articles',
		'events',
		'job_offers',
		'publications',
		'case_studies'
	),
	'post_status'    => 'publish',
	's'              => get_search_query(),
	'orderby'        => 'publish_date',
	'order'          => 'ASC',
	'posts_per_page' => 15, // post per page
	'paged'          => $paged,
) );

$num = $loop->found_posts;
$total_pages = $loop->max_num_pages;
?>

    <div class="wrapper">
        <div class="breadcrumbs">
			<li><a href="<?= get_home_url() ?>"></a> <?php _e('Home', 'Sciforma'); ?></li>
            <li> <?php _e('Search results for: ', 'Sciforma'); ?> <?= get_search_query() ?></li>
        </div>

        <div class="top-result"><h3><?= get_field('text_for_serach_1','option') ?>:
                <b> <?php echo $num; ?> <?= get_field('text_for_search_2','option') ?> </b></h3></div>
        <div class="list-result">
			<?php
			if ( $loop->have_posts() ) {

				while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <div class="item"><h2><a href="<?= get_permalink( get_the_ID() ) ?>"><?= get_the_title(); ?></a>
                        </h2></div>
				<?php

				endwhile;


				if ( $total_pages > 1 ) {
					$current_page = max( 1, get_query_var( 'paged' ) );
					$big          = 9999999;
					?>
                    <div class="pagination">
						<?php

						echo paginate_links( array(
							'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format'  => '?paged=%#%',
							'current' => $paged,
							'total'   => $loop->max_num_pages
						) );
						?>
                    </div>
					<?php
				}
			}
			wp_reset_postdata();
			wp_reset_query();
			?>
        </div>

    </div>

<?php get_footer(); ?>