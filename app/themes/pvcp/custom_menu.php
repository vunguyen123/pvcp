<?php
function sci_megamenu_nav_menu_args($default){
    $css = $default['container_class'];
    $default['container_class'] = $css.' menu ';
    $default['menu_class'] = $default['menu_class'].' menu' ;
    return $default;

}
add_filter('megamenu_nav_menu_args','sci_megamenu_nav_menu_args');

function sci_inline_css(){?>
	<style type="text/css">
		#mega-menu-wrap-athena_main_menu{
			margin-top: 4px !important;
		}
		#mega-menu-wrap-athena_main_menu #mega-menu-athena_main_menu > li.mega-menu-megamenu.mega-menu-item{
			position: initial !important;
		}
		#mega-menu-wrap-athena_main_menu #mega-menu-athena_main_menu > li.mega-menu-item > a.mega-menu-link{
			color: #2a2d33;
			font-size: 16px;
		}
		#mega-menu-wrap-athena_main_menu #mega-menu-athena_main_menu > li.mega-menu-item > a.mega-menu-link:hover{
			color: #4c69ff;
		}

		.mega-menu>li:hover>a::after, .mega-menu>li.active>a::after {
		    right: 0;
		}
		.max-mega-menu>li:hover>a::after,
		ul.mega-menu>li>a::after,
		ul.mega-menu>li.mega-menu-item::after {
		    content: '';
		    position: absolute;
		    left: 0;
		    right: 100%;
		    height: 0.3125rem;
		    bottom: 1px;
		    background-color: #4c69ff;
		    -webkit-transition: all .3s ease;
		    -o-transition: all .3s ease;
		    transition: all .3s ease;
		}
		span.mega-indicator{
			display: none;
		}
		#mega-menu-wrap-athena_main_menu #mega-menu-athena_main_menu li.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator{
			display: none;
		}
		@media only screen and (min-width: 769px) {
		  #mega-menu-wrap-athena_main_menu {
		    background: transparent;
		  }

		}
		#mega-menu-wrap-athena_main_menu #mega-menu-athena_main_menu > li.mega-menu-megamenu > ul.mega-sub-menu{
			background-color: #fff;
		}
		#mega-menu-wrap-athena_main_menu #mega-menu-athena_main_menu > li.mega-menu-item > a.mega-menu-link:hover{
			background: transparent;
		}
.max-mega-menu>li.mega-menu-item:hover .mega-sub-menu {
    opacity: 1;
    visibility: visible;
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
    pointer-events: auto;
}
#mega-menu-wrap-athena_main_menu #mega-menu-athena_main_menu li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row{
	z-index: 11;
}
#mega-menu-wrap-athena_main_menu #mega-menu-athena_main_menu > li.mega-menu-megamenu > ul.mega-sub-menu::after{
	content: '';
	display: block;
    position: absolute;
    top: 0;
    left: -100em;
    right: -100em;
    height: 100%;
    z-index: 0;
    background: #fff;
    -webkit-box-shadow: 0 1.25rem 1.875rem rgb(102 111 129 / 15%);
    box-shadow: 0 1.25rem 1.875rem rgb(102 111 129 / 15%);
}
#mega-menu-wrap-athena_main_menu #mega-menu-athena_main_menu > li.mega-menu-item > a.mega-menu-link{
	height: 90px;
	line-height: 90px;
	display: block;
	z-index: 3;
}

#mega-menu-wrap-athena_main_menu #mega-menu-athena_main_menu > li.mega-menu-item:hover > a.mega-menu-link::after,
#mega-menu-wrap-athena_main_menu #mega-menu-athena_main_menu > li.mega-menu-item:active > a.mega-menu-link::after {
	 right: 0;
}

#mega-menu-wrap-athena_main_menu #mega-menu-athena_main_menu > li.mega-menu-item > a.mega-menu-link::after {
    content: '';
    position: absolute;
    display: block;
    z-index: 10;
    left: 0;
    right: 100%;
    height: 0.3125rem;
    bottom: 0;
    background-color: #4c69ff;
    -webkit-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease;
}


	</style>
<?php }
add_action('wp_footer','sci_inline_css');

function sci_static_html_menu(){?>
<ul class="menu "   >
		<li class="menu-item-has-children">
			<a href="#">Pourquoi Sciforma ?</a>
			<ul class="sub-menu">
				<li class="menu-title">Pourquoi Sciforma ?</li>
				<li class="col-9">
					<ul class="menu-two-column">
						<li>
							<h4>Présentation</h4>
							<ul>
								<li>
									<h5><a href="#">Découvrir notre produit </a></h5>
									<p>Arcu dictum cursus at felis non. Velit dui volutpat.</p>
									<p><a href="#" class="link-more">Voir la présentation</a></p>
								</li>
								<li>
									<h5><a href="#">Nos réussites clients</a></h5>
									<p>Arcu dictum cursus at felis non. Velit dui volutpat.</p>
									<p><a href="#" class="link-more">Voir la présentation</a></p>
								</li>
							</ul>
						</li>
						<li>
							<h4>Par besoin</h4>
							<ul class="ul-two-column">
								<li>
									<ul>
										<li>
											<h5><a href="#">Améliorer la productivité</a></h5>
											<p>Grâce à la centralisation de vos données, accédez
												directement
												aux bonnes informations.</p>
										</li>
										<li>
											<h5><a href="#">Choisir les bons projets à lancer</a></h5>
											<p>Réservez vos budgets et vos ressources ayant le plus de
												valeur ajouté.</p>
										</li>
										<li>
											<h5><a href="#">Avantage</a></h5>
											<p>Laoreet arcu at lacus, blandit vitae egestas diam, sed
												volutpat.</p>
										</li>
									</ul>
								</li>
								<li>
									<ul>
										<li>
											<h5><a href="#">Optimiser l’emploi des ressources</a></h5>
											<p>Maîtrisez votre capacité à faire. Connaissez leur
												utilisation réelle.</p>
										</li>
										<li>
											<h5><a href="#">Améliorer le succès de vos projets</a></h5>
											<p>Réduisez la durée et les retards
												de vos projets.</p>
										</li>
										<li>
											<h5><a href="#">Avantage</a></h5>
											<p>Ut odio amet quisque iaculis.</p>
										</li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="col-3">
					<h4>Webinars <a href="#" class="link-more">Voir tout</a></h4>
					<div class="card-article">
						<div class="card-article__image circle">
							<img src="./assets/images/art-1.png" alt="art">
							<img src="./assets/images/art-thumb-1.png" alt="" class="img-thumb">
						</div>
						<h5><a href="#">Présentation du logiciel</a></h5>
						<p>Session live de démonstration de l’outil et de ses fonctionnalités
							principales avec <b>Alexis
								Kobagashi</b>, formateur Sciforma.</p>
						<p class="date">24 novembre - 10h00 - France</p>
						<p><a href="#" class="btn btn-outline-orange">S’inscrire à l’événement</a></p>
					</div>
				</li>
			</ul>
		</li>
		<li class="menu-item-has-children">
			<a href="#">Solutions</a>
			<ul class="sub-menu">
				<li class="menu-title">Solutions</li>
				<li class="col-9">
					<ul>
						<li>
							<h4>Par équipe</h4>
							<ul>
								<li>
									<h5><a href="#">Marketing</a></h5>
									<p>Arcu dictum cursus at felis non. Velit dui volutpat.</p>
								</li>
								<li>
									<h5><a href="#">Opérations</a></h5>
									<p>Arcu dictum cursus at felis non. Velit dui volutpat.</p>
								</li>
								<li>
									<h5><a href="#">Creative</a></h5>
									<p>Arcu dictum cursus at felis non. Velit dui volutpat.</p>
								</li>
								<li>
									<h5><a href="#">Développement</a></h5>
									<p>Arcu dictum cursus at felis non. Velit dui volutpat.</p>
								</li>
							</ul>
						</li>
						<li>
							<h4>Par besoin</h4>
							<ul>
								<li>
									<h5><a href="#">Gestion</a></h5>
									<p><a href="#" class="link-more arrow-left">Idées et demandes</a>
									</p>
									<p><a href="#" class="link-more arrow-left">Portefeuilles de
											projets</a></p>
									<p><a href="#" class="link-more arrow-left">Ressources et
											capacité</a></p>
									<p><a href="#" class="link-more arrow-left">Travail collaboratif</a>
									</p>
									<p><a href="#" class="link-more arrow-left">Planification de
											projets</a></p>
								</li>
								<li>
									<h5><a href="#">Suivi</a></h5>
									<p><a href="#" class="link-more arrow-left">Idées et demandes</a>
									</p>
								</li>
							</ul>
						</li>
						<li>
							<h4>Par secteur</h4>
							<ul>
								<li>
									<h5><a href="#">Banque et Services financiers</a></h5>
									<p>Arcu dictum cursus at felis non. Velit dui volutpat.</p>
								</li>
								<li>
									<h5><a href="#">Électrique et électronique</a></h5>
									<p>Arcu dictum cursus at felis non. Velit dui volutpat.</p>
								</li>
								<li>
									<h5><a href="#">Hôpitaux et santé</a></h5>
									<p>Arcu dictum cursus at felis non. Velit dui volutpat.</p>
								</li>
								<li>
									<h5><a href="#">Pharma et Biotechnologie</a></h5>
									<p>Arcu dictum cursus at felis non. Velit dui volutpat.</p>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="col-3">
					<h4>Modèles</h4>
					<div class="card-article">
						<h5><a href="#">“Out of the box”</a></h5>
						<p>Sciforma est un outil facile à déployer et à maintenir. Découvrez nos modèles
							les plus utilisés,
							quelque soit la taille de votre entreprise.</p>
						<div class="list-item">
							<div class="item">
								<div class="image">
									<img src="./assets/images/icons/GlobeHemisphereEast.svg" alt="icon">
								</div>
								<h5 class="title-item">Modèle Grand groupe</h5>
							</div>
							<div class="item">
								<div class="image">
									<img src="./assets/images/icons/Factory.svg" alt="icon">
								</div>
								<h5 class="title-item">Modèle TPE</h5>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</li>
		<li class="menu-item-has-children">
			<a href="#">Clients</a>
			<ul class="sub-menu">
				<li class="menu-title">Clients</li>
				<li class="col-8">
					<ul class="menu-two-column">
						<li>
							<h4>Présentation</h4>
							<ul>
								<li>
									<h5><a href="#">Découvrir notre produit </a></h5>
									<p>Arcu dictum cursus at felis non. Velit dui volutpat.</p>
								</li>
								<li>
									<h5><a href="#">Nos réussites clients</a></h5>
									<p>Arcu dictum cursus at felis non. Velit dui volutpat.</p>
								</li>
							</ul>
						</li>
						<li>
							<h4>Secteurs principaux <a href="#" class="link-more">Voir tout</a></h4>
							<ul class="ul-two-column">
								<li>
									<ul>
										<li>
											<h5><a href="#">Améliorer la productivité</a></h5>
											<p>Grâce à la centralisation de vos données, accédez
												directement
												aux bonnes informations.</p>
										</li>
										<li>
											<h5><a href="#">Choisir les bons projets à lancer</a></h5>
											<p>Réservez vos budgets et vos ressources ayant le plus de
												valeur ajouté.</p>
										</li>
										<li>
											<h5><a href="#">Avantage</a></h5>
											<p>Laoreet arcu at lacus, blandit vitae egestas diam, sed
												volutpat.</p>
										</li>
									</ul>
								</li>
								<li>
									<ul>
										<li>
											<h5><a href="#">Optimiser l’emploi des ressources</a></h5>
											<p>Maîtrisez votre capacité à faire. Connaissez leur
												utilisation réelle.</p>
										</li>
										<li>
											<h5><a href="#">Améliorer le succès de vos projets</a></h5>
											<p>Réduisez la durée et les retards
												de vos projets.</p>
										</li>
										<li>
											<h5><a href="#">Avantage</a></h5>
											<p>Ut odio amet quisque iaculis.</p>
										</li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="col-4">
					<h4>À l’honneur</h4>
					<div class="card-article">
						<div class="card-article__image">
							<img src="./assets/images/art-1.png" alt="art">
							<img src="./assets/images/logo-societe-generale.png" alt=""
							     class="img-thumb">
						</div>
						<p>Société Générale Algérie : renforcer
							la rigueur de la gestion de projets
							pour toujours mieux servir les clients internes</p>
						<p><a href="#" class="btn btn-outline-orange">Voir l’étude de cas</a></p>
					</div>
				</li>
			</ul>
		</li>
		<li class="menu-item-has-children">
			<a href="#">Ressources</a>
			<ul class="sub-menu">
				<li class="menu-title">Ressources</li>
				<li class="col-8">
					<ul>
						<li>
							<h4>Ressources</h4>
							<ul>
								<li>
									<p><a href="#" class="link-more arrow-left">Blog</a></p>
									<p><a href="#" class="link-more arrow-left">E-books</a></p>
									<p><a href="#" class="link-more arrow-left">Replays</a></p>
									<p><a href="#" class="link-more arrow-left">FAQ</a></p>
								</li>
							</ul>
						</li>
						<li>
							<h4>Événements <a href="#" class="link-more">Voir tout</a></h4>
							<ul>
								<li>
									<p><a href="#" class="link-more arrow-left">Webinars</a></p>
									<p><a href="#" class="link-more arrow-left">Salons et
											conférences</a></p>
									<p><a href="#" class="link-more arrow-left">Événements Club
											utilisateurs</a></p>
								</li>
							</ul>
						</li>
						<li>
							<h4>Formation</h4>
							<ul>
								<li>
									<p><a href="#" class="link-more arrow-left">Webinars</a></p>
									<p><a href="#" class="link-more arrow-left">Salons et
											conférences</a></p>
									<p><a href="#" class="link-more arrow-left">Événements Club
											utilisateurs</a></p>
									<p><a href="#" class="link-more arrow-left">Événements Club
											utilisateurs</a></p>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="col-4">
					<h4>Livre blanc</h4>
					<div class="card-article">
						<div class="card-article__image">
							<img src="./assets/images/art-1.png" alt="art">
						</div>
						<p><a href="#" class="tag orange">Livre blanc</a></p>
						<h5><a href="#">Présentation du logiciel</a></h5>
						<p>Coopératives agricoles, savez-vous
							ce qu’un logiciel PPM peut faire pour vous ?</p>
						<p><a href="#" class="btn btn-outline-orange">Télécharger</a></p>
					</div>
				</li>
			</ul>
		</li>
		<li class="menu-item-has-children">
			<a href="#">Entreprise</a>
			<ul class="sub-menu">
				<li class="menu-title">Entreprise</li>
				<li class="col-8">
					<ul>
						<li>
							<h4>Ressources</h4>
							<ul>
								<li>
									<p><a href="#" class="link-more arrow-left">Blog</a></p>
									<p><a href="#" class="link-more arrow-left">E-books</a></p>
									<p><a href="#" class="link-more arrow-left">Replays</a></p>
									<p><a href="#" class="link-more arrow-left">FAQ</a></p>
								</li>
							</ul>
						</li>
						<li>
							<h4>Événements <a href="#" class="link-more">Voir tout</a></h4>
							<ul>
								<li>
									<p><a href="#" class="link-more arrow-left">Webinars</a></p>
									<p><a href="#" class="link-more arrow-left">Salons et
											conférences</a></p>
									<p><a href="#" class="link-more arrow-left">Événements Club
											utilisateurs</a></p>
								</li>
							</ul>
						</li>
						<li>
							<h4>Formation</h4>
							<ul>
								<li>
									<p><a href="#" class="link-more arrow-left">Webinars</a></p>
									<p><a href="#" class="link-more arrow-left">Salons et
											conférences</a></p>
									<p><a href="#" class="link-more arrow-left">Événements Club
											utilisateurs</a></p>
									<p><a href="#" class="link-more arrow-left">Événements Club
											utilisateurs</a></p>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="col-4">
					<h4>Sciforma, c’est aussi</h4>
					<div class="card-article">
						<div class="list-item">
							<div class="item number">
								<h5 class="title-item">40 ans</h5>
								<p>d’expérience</p>
							</div>
							<div class="item number">
								<h5 class="title-item">320</h5>
								<p>collaborateurs</p>
							</div>
							<div class="item number">
								<h5 class="title-item">30 000</h5>
								<p>utilisateurs</p>
							</div>
							<div class="item number">
								<h5 class="title-item">12</h5>
								<p>pays</p>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</li>
	</ul>
<?php }
