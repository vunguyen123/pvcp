<?php
/**
 * Enqueue scripts and styles.
 **/
function athena_scripts() {
	// Styles
	wp_enqueue_style( 'main-style', ASSETS_PATH . 'css/main.css', array(), null );

	// Scripts
	wp_enqueue_script( 'main-script', 'https://code.jquery.com/jquery-3.6.0.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'custom-script', ASSETS_PATH . 'js/main.js', array( 'jquery' ), null, true );

	wp_localize_script( 'main-script', 'wp_localize',
		array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'homeurl' => get_home_url()
		)
	);
	wp_enqueue_script( 'main-script' );

}

add_action( 'wp_enqueue_scripts', 'athena_scripts' );

add_image_size( 'image-accroche', 770, 595, true );
add_image_size( 'image-card', 360, 505, true );
add_image_size( 'image-slider', 550, 355, true );
add_image_size( 'image-join-community', 375, 100, true );
add_image_size( 'image-post-detail', 1140, 590, true );
add_image_size( 'image-post-relationship', 300, 180, true );
add_image_size( 'image-custom-block-list', 445, 280, true );
add_image_size( 'image-custom-publications', 330, 200, true );
add_image_size( 'avatar', 50, 50, true );
add_image_size( 'custom_location_detail', 178, 180, true );

/**
 * Register Menu
 **/
add_action( 'init', 'athena_setup' );
function athena_setup() {
	register_nav_menus( array(
		'athena_main_menu' => __( 'Main Menu', DOMAIN )
	) );
	add_theme_support( 'post-thumbnails' );
	add_post_type_support( 'page', 'excerpt' );
}

if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page( array(
		'page_title' => 'Theme General Settings',
		'menu_title' => 'Theme Settings',
		'menu_slug'  => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect'   => false
	) );
}

function cc_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';

	return $mimes;
}

add_filter( 'upload_mimes', 'cc_mime_types' );
