<?php

/**
 * ACF Functions
 **/

//load_json
add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point( $paths ) {

	// remove original path (optional)
	unset($paths[0]);

	// append path
	$paths[] = get_stylesheet_directory() . '/acf-json';

	// return
	return $paths;

}

add_action('acf/init', 'athena_acf_init');
function athena_acf_init() {

	// check function exists
	if( function_exists('acf_register_block') ) {

		// register a Home banner block
		acf_register_block(array(
			'name'				=> 'title-icon',
			'title'				=> __('Block title and icon'),
			'description'		=> __('A custom title and icon block.'),
			'render_callback'	=> 'athena_acf_block_render_callback',
			'category'			=> 'home-blocks',
			'icon'				=> 'format-aside',
			'keywords'			=> array( 'Detail post', 'title and icon' ),
		));


		// register a Home banner block
		acf_register_block(array(
			'name'				=> 'list-image-right',
			'title'				=> __('Block list and image right'),
			'description'		=> __('A custom list image right block.'),
			'render_callback'	=> 'athena_acf_block_render_callback',
			'category'			=> 'home-blocks',
			'icon'				=> 'format-aside',
			'keywords'			=> array( 'Detail post', 'list image' ),
		));


		// register a Home banner block
		acf_register_block(array(
			'name'				=> 'button-custom',
			'title'				=> __('Block custom button'),
			'description'		=> __('A custom list image right block.'),
			'render_callback'	=> 'athena_acf_block_render_callback',
			'category'			=> 'home-blocks',
			'icon'				=> 'format-aside',
			'keywords'			=> array( 'Detail post', 'custom button' ),
		));

		// register list number
		acf_register_block(array(
			'name'				=> 'list-number',
			'title'				=> __('Block list number'),
			'description'		=> __('A custom list number block.'),
			'render_callback'	=> 'athena_acf_block_render_callback',
			'category'			=> 'home-blocks',
			'icon'				=> 'format-aside',
			'keywords'			=> array( 'List number', 'list number' ),
		));


	}
}

//load template for block
function athena_acf_block_render_callback( $block ) {
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);

	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/block/content-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/block/content-{$slug}.php") );
	}
}